# Near Future Electrical : UNOFFICIAL

This is a unofficial, non authorized repository for **Near Future Electrical** for historical reference and troubleshooting.


## In a Hurry

* [Binaries](https://github.com/net-lisias-ksph/NearFutureElectrical/tree/CC-BY-NC-SA-4.0/Archive)
* [Sources](https://github.com/net-lisias-ksph/NearFutureElectrical/tree/CC-BY-NC-SA-4.0/Master)
* [Change Log](./CHANGE_LOG.md)


## Description

Ever wanted some futuristic energy generation that wasn't too... futuristic? I have you covered here. This pack contains:

* **Nuclear Reactors**: Turn Uranium into power! Attach nuclear reactors to your vessel and generate large amount of electricity. Ensure that you have enough cooling capacity with radiators for your reactors to work properly.  
* **Refuelling Parts**: store extra uranium, reprocess it and extract it from Ore with containers and reprocessors.
* **Capacitors**: discharge for a burst of power! These parts can be charge up with normal generating capacity, and once activated will deliver a large amount of power to your ship's electricity banks. Very mass efficient!


## License

### For Releases <= 0.8.2

This AddOn is (C) [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/) and licensed under [CC BY NC SA](https://creativecommons.org/licenses/by-nc-sa/4.0/), what allows you to:

* copy and redistribute the material in any medium or format
* remix, transform, and build upon the material for any purpose, even commercially.

As long you

* give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
* you don't use the material for commercial purposes.
* if you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
* You don't

You are authorized to fork this repository under GitHub [ToS](https://help.github.com/articles/github-terms-of-service/) **and** the License above.

### For Releases >= 0.8.3

This AddOn is (C) [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/). All Right Reserved.

This repository is under the claim of the [right to backup](https://info.legalzoom.com/copyright-law-making-personal-copies-22200.html) and should be considered private:

> Copyright law permits you to make one copy of your computer software for the purpose of archiving the software in case it is damaged or lost. In order to make a copy, you must own a valid copy of the software and must destroy or transfer your backup copy if you ever decide to sell, transfer or give away your original valid copy. It is not legal to sell a backup copy of software unless you are selling it along with the original.

I grant you no rights on any artifact on this repository, unless you own yourself the right to use the software and authorizes me to keep backups for you:

> (a) Making of Additional Copy or Adaptation by Owner of Copy. -- Notwithstanding the provisions of section 106, it is not an infringement for the owner of a copy of a computer program to make or authorize the making of another copy or adaptation of that computer program provided:
> 
>> (2) that such new copy or adaptation is for archival purposes only and that all archival copies are destroyed in the event that continued possession of the computer program should cease to be rightful.

[17 USC §117(a)](https://www.law.cornell.edu/uscode/text/17/117)

By using this repository without the required pre-requisites, I hold you liable to copyright infringement.


## References

* [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/)
	+ [KSP Forum](https://forum.kerbalspaceprogram.com/index.php?/topic/155465-*/)
	+ [imgur](https://imgur.com/a/JaN1h)
	+ [CurseForge](https://kerbal.curseforge.com/projects/near-future-electrical?gameCategorySlug=ksp-mods&projectID=220671)
	+ [SpaceDock](http://spacedock.info/mod/558/Near%20Future%20Electrical)
	+ [GitHub](https://github.com/ChrisAdderley/NearFutureElectrical/releases)
