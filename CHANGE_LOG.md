# Near Future Electrical :: Change Log

* 2021-0721: 1.2.3 (Nertea) for KSP 1.12.0
	+ KSP 1.12
	+ Updated B9PartSwitch to 2.18.0
	+ Updated DynamicBatteryStorage to 2.2.4
	+ RadioisotopeGenerator and FissionGenerator modules now count for contract objective completion
* 2021-0318: 1.2.2 (Nertea) for KSP 1.11.1
	+ Updated DynamicBatteryStorage to 2.2.2
	+ Reverted some USI patch changes
	+ RTG half life is now calculated correctly for other star systems, config value is specifically in Kerbin years.
	+ Reactor life display is now calculated correctly for other star systems
	+ Some code cleanup
* 2020-1227: 1.2.1 (Nertea) for KSP 1.11.0
	+ Fix for inventory parts in 1.10
* 2020-1223: 1.2.0 (Nertea) for KSP 1.11.0
	+ KSP 1.11
	+ Updated DynamicBatteryStorage to 2.2.1
	+ Updated CRP to 1.4.2
	+ Set many parts as usable in inventories roughly following stock model: most 0.625m parts, smaller 1.25m parts
* 2020-0803: 1.1.3 (Nertea) for KSP 1.10.1
	+ KSP 1.10
	+ Updated B9PartSwitch to 2.17.0
	+ Updated DynamicBatteryStorage to 2.1.7
	+ Updated ModuleManager to 4.1.4
	+ Updated CRP to 1.3.0 (Really this time)
* 2020-0526: 1.1.2 (Nertea) for KSP 1.9.1
	+ Updated B9PartSwitch to 2.16.0
	+ Updated DynamicBatteryStorage to 2.1.6
	+ Updated USI compatibility patch
* 2020-0220: 1.1.1 (Nertea) for KSP 1.9.0
	+ Marked for KSP 1.9
	+ Updated B9PartSwitch to 2.13.0
	+ Updated DynamicBatteryStorage to 2.1.5
	+ Updated ModuleManager to 4.1.3
	+ Updated Russian localization (Sool3)
	+ Updated Simplified Chinese localization (tinygrox)
* 2019-1106: 1.1.0 (Nertea) for KSP 1.8.1
	+ KSP 1.8
	+ Rebuild with .NET 4.5
	+ Updated B9PartSwitch to 2.12.1
	+ Updated DynamicBatteryStorage to 2.1.0
	+ Updated ModuleManager to 4.1.0
	+ Updated CRP to 1.3.0
	+ DEPRECATED OLD 3.75m BATTERY
	+ Fixed another case where the stock heat catchup system was not being correctly intercepted on load for reactors.
	+ Updated all patches to use correct MM patch pass specifier
* 2019-0911: 1.0.3 (Nertea) for KSP 1.7.0
	+ Updated B9PartSwitch to 2.10.0
	+ Updated DynamicBatteryStorage to 2.0.6
	+ Made use of B9PS 2.9+ features for reactors (colours, labels)
* 2019-0709: 1.0.2 (Nertea) for KSP 1.7.0
	+ Updated B9PartSwitch to 2.8.0
	+ Updated DynamicBatteryStorage to 2.0.0
		- Complete rebuild
		- Mod now acts as a general power and thermal design tool as well as its previous functions
		- Added toolbar icon to open a Vessel Systems Management window
		- Vessel Systems Management Window
			- Allows player to view relevant Thermal and Electrical summary of the current vessel in VAB or flight
			- Electrical summary:
				- Shows whole-vessel power flows, separated into consumption and generation
				- VAB panel has a tool to simulate distance effects on solar panel efficiency
				- Estimates time to drain or time to charge batteries
				- Can drill down to part categories (eg. Solar Panels, Harvesters, etc)
				- Can drill down to individual parts
				- Handles these module types
					- Stock: ModuleDeployableSolarPanel, ModuleGenerator, ModuleResourceConverter, ModuleActiveRadiator, ModuleResourceHarvester, ModuleCommand, ModuleLight, ModuleDataTransmitter, ModuleEnginesFX, ModuleAlternator
					- NF Suite: ModuleCurvedSolarPanel, FissionGenerator, ModuleRadioisotopeGenerator, ModuleCryoTank, ModuleAntimatterTank, ModuleChargeableEngine, ModuleDeployableCentrifuge, DischargeCapacitor (partial)
					- RealBattery: RealBattery
					- Other: KopernicusSolarPanel
			- Thermal mode:
				- Shows whole-vessel core heat flows, separated into draw and generation
				- Can drill down to part categories (eg. Solar Panels, Harvesters, etc)
				- Can drill down to individual parts
				- NOTE: does not handle non-core heat(eg re-entry, engines, solar)
				- NOTE: does not make a distinction between adjacent-only radiators and full-vessel radiators
				- Handles relevant module types
					- Stock: ModuleCoreHeat (Passive heat only eg RTG), ModuleResourceConverter, ModuleActiveRadiator, ModuleResourceHarvester
					- NF Suite: FissionReactor, ModuleFusionCore
		- Customize settings with DynamicBatteryStorageSettings.cfg
	+ Fixed an older dll being shipped with the mod
	+ Capacitor recharge can now be enabled or disabled in the VAB
* 2019-0417: 1.0.1 (Nertea) for KSP 1.7.0
	+ KSP 1.7.x rebuild
	+ Updated DynamicBatteryStorage to 1.4.1
	+ Updated B9PartSwitch to 2.7.0
* 2019-0401: 1.0.0 (Nertea) for KSP 1.6.1
	+ Final content update
	+ Updated ModuleManager to 4.0.2
	+ Some restructuring of files and assets, ensure you delete old NFE installations
	+ 3.75m battery was soft-deprecated, if you need one, there is a better one in Restock+
	+ Capacitors were completely remodeled and retextured
	+ Batteries were completely remodeled and retextured
	+ Nuclear fuel containers were retextured
	+ Nuclear reprocessor was partially remodeled and fully retextured
	+ ASRTG was retextured
	+ Tuned textures for reactors to be more consistent with Restock palette
	+ Most textures recompressed from source with higher quality compressor
	+ Capacitor module no longer uses legacy animations for indicator lights, uses ModuleColorChanger instead
	+ Nuclear fuel containers now animate lights depending on amount of fuel or waste in the container
* 2019-0121: 0.10.6 (Nertea) for KSP 1.6.1
	+ Add bulkhead profiles to missing parts (thanks Streetwind)
	+ Added German translation courtesy of Three_Pounds
* 2019-0117: 0.10.5 (Nertea) for KSP 1.6.1
	+ KSP 1.6.x
	+ Updated B9PartSwitch to 2.6.0
	+ Updated ModuleManager to 3.1.3
	+ Updated DynamicBatteryStorage to 1.4.0
	+ Small change to license of code/configs (MIT)
* 2018-1116: 0.10.4 (Nertea) for KSP 1.5.1
	+ Fixed an exploit where you could button mash the discharge capacitor button constantly for a net gain of EC
	+ Increased the minimum core temperature allowed for radioactive fuel transfer to 450 K
* 2018-1105: 0.10.3 (Nertea) for KSP 1.4.4
	+ KSP 1.5.x
	+ Updated B9PartSwitch to 2.4.5
	+ Updated CRP to 1.0.0
	+ Updated ModuleManager to 3.1.0
	+ Updated DynamicBatteryStorage to 1.3.3
	+ Removed MiniAVC
* 2018-0807: 0.10.2 (Nertea) for KSP 1.4.2
	+ KSP 1.4.5
	+ Updated B9PartSwitch to 2.3.3
	+ Updated ModuleManager to 3.0.7
	+ Updated DynamicBatteryStorage to 1.3.2
	+ Fixed regression for fuel transfer localization
* 2018-0503: 0.10.1 (Nertea) for KSP 1.4.2
	+ KSP 1.4.3
	+ Updated B9PartSwitch to 2.3.0
	+ Switched versioning to mix/max specification
	+ Fixed a bug with fuel transfer that caused it to basically never work
* 2018-0410: 0.10.0 (Nertea) for KSP 1.3.1
	+ Updated B9PartSwitch to 2.1.1
	+ Updated ModuleManager to 3.0.6
	+ Updated MiniAVC to 1.2.0.1
	+ Updated CRP to 0.10.0
	+ Added Portugese localization (GabrielGABFonesca)
	+ Fixes to Russian localization
	+ Add reactor nicknames to part search tags (kerbas_ad_astra)
	+ Capacitors can be manually discharged on EVA
	+ Changed fuel transfer behaviour to ignore the temperature requirement unless the part has a Core
	+ Fixed fuel transfer under other localizations again, maybe
	+ Misc fixes to thermal behaviour of reactors
	+ Improvements to KA/NFE patch
	+ Engine reactors don't suck up radiator power
* 2017-1128: 0.9.8 (Nertea) for KSP 1.3.1
	+ Updated B9PartSwitch to 2.0.0
	+ Fixed an possible issue with reactor radiator consumption
	+ Updated DynamicBatteryStorage to 1.2.0
		- Fixed a bug that caused the buffer to be destroyed every second time it was created
		- Fixed solar panel handling when Kopernicus is installed
* 2017-1016: 0.9.7 (Nertea) for KSP 1.3.1
	+ Fixed a bundling issue, actually resolving with NFE patch, if integrated engine is inactive, disable auto-adjustment of reactor throttle
* 2017-1013: 0.9.6 (Nertea) for KSP 1.3.0
	+ KSP 1.3.1
	+ Dependency updates
	+ Fixed localization breaking nuclear fuel transfer and reactor repair
	+ With NFE patch, if integrated engine is inactive, disable auto-adjustment curve
* 2017-0731: 0.9.5 (Nertea) for KSP 1.3.0
	+ Added Chinese translation courtesy of DYZBX from the Black Domain Studio(BDS)
	+ Added Russian translation courtesy of Dr. Jet
	+ Fixed normal map channels
	+ Fixed a localization error on fuel transfer messages
	+ Fixed a localization error on destroyed reactor cores
	+ Fixed a few missing units in UI fields
	+ Modified units in UI fields and panels to be localized
	+ Fixed an issue where reactors would overheat after restart after timewarp (I think...)
	+ Fixed FissionEngine not caring about core damage
	+ Updated DynamicBatteryStorage to 1.1.0
	+ Refactored plugin for many improvements
	+ Proper support for RealBattery
* 2017-0712: 0.9.4 (Nertea) for KSP 1.3.0
	+ Updated B9PartSwitch to 1.9.0
	+ Updated ModuleManager to 2.8.1
	+ Fixed low cost of B-800 radial battery pack
	+ Improvements to NFE/KA integration
	+ Tweak a few localization fields
* 2017-0630: 0.9.3 (Nertea) for KSP 1.3.0
	+ Hotfix paths
* 2017-0630: 0.9.2 (Nertea) for KSP 1.3.0
	+ Updated DynamicBatteryStorage to 1.0.1
	+ Reduced logging
	+ Fixed an issue with generator parsing on ModuleResourceConverters
* 2017-0626: 0.9.1 (Nertea) for KSP 1.3.0
	+ Updated B9PartSwitch to 1.8.1
	+ Added Spanish localization courtesy of forum user fitiales
	+ Fixed a small bug in the localization file
* 2017-0616: 0.9.0 (Nertea) for KSP 1.2.2
	+ KSP 1.3
	+ Updated bundled MM to 2.8.0
	+ Updated bundled B9PartSwitch to 1.8.0
	+ Updated bundled CRP to 0.7.1
	+ Full localization support for parts and plugins
	+ New sub-plugin: DynamicBatteryStorage
	+ Dynamically adjusts EC storage to combat KSP crap resource mechanics at high timewarp
	+ Should dramatically reduce instances of EC loss at high timewarp
	+ Revised many part descriptions, tags and names
	+ Revised costs of batteries to correctly match stock batteries
	+ Revised costs of capacitors to be slightly more expensive (~1.5x) than before
	+ Battery and capacitor mass values are now 100% consistent
	+ Reactor numbering system revised for better sorting
	+ All reactors are now their inline variant by default
	+ MX-3L Hermes reactor now generates 6000 kW, up from 5000 kW. All other stats adjusted to match
	+ Rebalanced cost and mass of all reactors
	+ Reduced electricity storage of all reactors to 50% of kW
	+ Rebalanced mass and cost of all nuclear fuel containers
	+ Reduced price of PB-AS-NUK RTG
	+ Increased mass of Whirlijig Nuclear Reprocessor
	+ Adjusted recipes for Whirlijig reprocessing/extraction to be less weird
	+ DecayingRTGs patch now cuts price of affected RTGs by 50%
	+ Changed CLS patch to behave better when CLS is not around
	+ Cleaned up FissionReactor config blocks for clarity
	+ Significant improvements to KA integration code
* 2017-0322: 0.8.7 (Nertea) for KSP 1.2.2
	+ New 3.75m MX-5 'Hermes' nuclear reactor, generates 5000 kW of electricity
	+ New model for 3.75m FLAT nuclear reactor
	+ Radial capacitors are now physicsless
	+ Capacitors now have a cfg-configurable DischargeRateMinimumScalar field that can be used to edit the minimum discharge rate of a capacitor (previously locked at 50%)
	+ Reactors now have a new AutoWarpShutdown toggle which allows the user to set a reactor so that it gets shut down at warp factors greater or equal to the specified number
		- Capacitor UI rewrite:
		- Significant visual improvements
	+ Fixed a bug that caused capacitor rate setting in the UI to be erronous.
		- Reactor UI rewrite:
		- Significant visual improvements
		- Reactors can now be given a persistent custom icon and name
		- Better data display and controls
	+ Advanced control mode allowing the setting of the AutoShutdown temperature and a new AutoShutdown warp factor
		- Added more RTGs to DecayingRTGs patch courtesy of OrenWatson
		- Added new USI 1.25m reactor to USI patch, courtesy of Wyzard256
* 2017-0310: 0.8.6 (Nertea) for KSP 1.2.2
	+ Fixed animation corruption on Whirlijig model
	+ Fixed obsolete normal map on Whirlijig model
* 2017-0309: 0.8.5 (Nertea) for KSP 1.2.2
	+ Changed surface attach mode of Whirlijig to allow surface attachments on some areas of the part
	+ Fixed USI Patch PDU fuel rate
* 2017-0208: 0.8.4 (Nertea) for KSP 1.2.2
	+ Update B9PartSwitch to 1.7.1
	+ Updated CRP to 0.6.6
	+ Fixed download link in .version file
	+ Fixed costs of nuclear fuel containers
	+ Fixed a rogue logging spam
	+ Fixed a null reference exception when starting a reactor for the first time
* 2017-0123: 0.8.3 (Nertea) for KSP 1.2.1
	+ Marked for KSP 1.2.2
	+ Updated bundled MM to 2.7.5
	+ Updated bundled B9PartSwitch to 1.5.3
	+ Updated bundled CRP to 0.6.4
	+ Fixed capacitor discharge rate not being correct
	+ Fixed nuclear reactors not properly adjusting their fuel use
	+ Spelling/comprehension fixes
	+ Significant improvements, fixes to USI integrations from Wyzard256
* 2016-1118: 0.8.2 (Nertea) for KSP 1.2
	+ Marked for KSP 1.2.1
	+ Updated bundled MM to 2.7.4
	+ Updated bundled CRP
	+ Fixed reactor UI panel's hardcoded max safety shutdown temp (adapts to reactor)
	+ Fixed auto-shutdown temperature resetting on revert to VAB
	+ Capacitors now recharge in the background if ship EC is > 25%
	+ Capacitor discharge slider is now directly tied to discharge instead of being a percentage
	+ FissionFlowRadiator now shows current cooling in its UI
	+ FissionFlowRadiator cooling is now more forgiving and linear
	+ FissionReactor auto-throttle obeys engine tweakable throttle
	+ New models for all nuclear fuel containers
* 2016-1026: 0.8.1 (Nertea) for KSP 1.2
	+ Updated CRP to v6.0.1
	+ Reactor safety slider will now auto-adjust to the meltdown temperature of the reactor
	+ Improved the VAB GUI display of reactor attributes
	+ Improved the VAB GUI display of exhaust cooling attributes
* 2016-1021: 0.8.0 (Nertea) for KSP 1.1.3
	+ KSP 1.2
	+ Some performance and code improvements
	+ Removed Radioactivity patch (should really be *in* Radioactivity)
	+ Updated MM
	+ Updated CRP
	+ Updated B9PartSwitch
* 2016-0901: 0.7.8 (Nertea) for KSP 1.1.3
	+ Fixed some bugs with reactor repair
	+ Added a MM patch for Radioactivity development
* 2016-0722: 0.7.7 (Nertea) for KSP 1.1.3
	+ Many improvements to the handling of the capacitor and reactor UI panels
* 2016-0715: 0.7.6 (Nertea) for KSP 1.1.3
	+ USI reactor patch will now operate correctly even if the old patch is present
	+ Improvments to KerbalAtomics NTR patch mechanics courtesy of henrybauer
	+ Added more RTGs to the DecayingRTGs patch
	+ Reactor/Capacitor UIs won't appear if there are no reactors or capacitors on the craft
* 2016-0626: 0.7.5 (Nertea) for KSP 1.1.2
	+ KSP 1.1.3
	+ Updated bundled CRP to 0.5.4
	+ Updated bundled B9PartSwitch to 1.4.3
	+ Added conversion patch for USI Core reactors
	+ Added a first pass at the Reactor UI
	+ Added toolbar icons to access the Capacitor UI and Reactor UI
	+ Added the ability to toggle the Capacitor UI with a hotkey
	+ Fixed a couple minor bugs in transfers for reactor fuel
* 2016-0527: 0.7.4 (Nertea) for KSP 1.1.2
	+ Fix dll issue
* 2016-0527: 0.7.3 (Nertea) for KSP 1.1.2
	+ Updated MM version
	+ Updated B9PartSwitch version
	+ Added revised model for Excalibur reactor
	+ Added colliders to "new" reactor models' structural areas when the structural node is enabled
	+ Fixed a bug where reactors would auto-shutdown on game load when running
	+ Core safety override now starts in the editor set to just over the reactor's nominal temperature
	+ FissionEngine is now tied to reactor throttle; for full thrust power the reactor must be running as well as hot
	+ FissionFlowRadiator no longer needs to be activated and has its on/off UI switch removed
* 2016-0518: 0.7.2 (Nertea) for KSP 1.1.2
	+ Update CRP version
* 2016-0511: 0.7.1 (Nertea) for KSP 1.1
	+ KSP 1.1.2
	+ Updated B9PartSwitch version
	+ Updated ModuleManager version
* 2016-0422: 0.7.0 (Nertea) for KSP 0.7.3
	+ No changelog provided
* 2016-0220: 0.6.2 (Nertea) for KSP 0.7.3
	+ Updated CRP version
	+ Improvements to stability of power generation at high warp
	+ Support for fancy NTRs if KerbalAtomics is installed
